#!/usr/bin/env bash
set -exuo pipefail
brew install \
  awscli \
  coreutils \
  dog \
  duf \
  exiftool \
  ffmpeg \
  ghostscript \
  git \
  htop \
  jq \
  libvterm \
  mosh \
  neovim \
  nmap \
  pandoc \
  tmux \
  tree \
  tty-clock \
  watch \
