#!/usr/bin/env bash
set -exuo pipefail
cargo install \
    bat \
    bottom \
    cargo-edit \
    cargo-info \
    cargo-update \
    cargo-watch \
    du-dust \
    exa \
    fd-find \
    flamegraph \
    fnm \
    git-delta \
    gping \
    hexyl \
    hyperfine \
    just \
    miniserve \
    ripgrep \
    tokei \
    topgrade \
    viu \
    zoxide \
    xh \
